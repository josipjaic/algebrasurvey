import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { SurveyComponent } from "./components/SurveyJS_Wrappers/survey.component";
import { SurveyCreatorComponent } from "./components/SurveyJS_Wrappers/survey.creator.component";
import { SurveyAnalyticsComponent } from "./components/SurveyJS_Wrappers/survey.analytics.component";
import { RouterModule } from "@angular/router";
import { TopBarComponent } from "./components/top-bar/top-bar.component";

@NgModule({
  declarations: [
    AppComponent,
    SurveyComponent,
    SurveyCreatorComponent,
    SurveyAnalyticsComponent,
    TopBarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([{ path: "", component: AppComponent }])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
